package stan_test

import (
	"testing"

	"bitbucket.org/ValentinLethiot/natsPoc/infrastructure/stan"
	"github.com/stretchr/testify/assert"
)

func TestStanSubscriberConnection(t *testing.T) {
	//Given

	//When
	subscriber, err := stan.NewStanSubscriber("test-cluster", "test")

	//Then
	assert.NoError(t, err)
	assert.NotNil(t, subscriber)
}

func TestStanSubscriberClose(t *testing.T) {
	//Given
	subscriber, _ := stan.NewStanSubscriber("test-cluster", "test3")

	//When
	err := subscriber.Close()

	//Then
	assert.NoError(t, err)
}
