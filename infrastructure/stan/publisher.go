package stan

import (
	"encoding/json"

	"bitbucket.org/ValentinLethiot/natsPoc/domain"
	"github.com/nats-io/stan.go"
)

type StanPublisher struct {
	Client stan.Conn
}

func NewStanPublisher(clusterID, clientID string) (*StanPublisher, error) {
	conn, err := stan.Connect(clusterID, clientID)
	if err != nil {
		return nil, err
	}
	return &StanPublisher{
		Client: conn,
	}, nil
}

func (publisher *StanPublisher) Publish(topic string, events ...*domain.Event) error {
	for _, event := range events {
		jsonEvent, err := json.Marshal(event)
		if err != nil {
			return err
		}

		err = publisher.Client.Publish(topic, jsonEvent)
		if err != nil {
			return err
		}
	}
	return nil
}

func (publisher *StanPublisher) Close() error {
	return publisher.Client.Close()
}
