package stan

import (
	"encoding/json"
	"sync"

	"bitbucket.org/ValentinLethiot/natsPoc/domain"
	"github.com/nats-io/stan.go"
)

type StanSubscriber struct {
	Client       stan.Conn
	lastReceived uint64
}

func NewStanSubscriber(clusterID, clientID string) (*StanSubscriber, error) {
	conn, err := stan.Connect(clusterID, clientID)
	if err != nil {
		return nil, err
	}
	return &StanSubscriber{
		Client:       conn,
		lastReceived: 0,
	}, nil
}

func (subscriber *StanSubscriber) Subscribe(topic string) (<-chan *domain.Event, error) {
	exitChannel := make(chan *domain.Event)
	go func(chan *domain.Event) {
		var event domain.Event
		subscriber.Client.Subscribe(topic, func(m *stan.Msg) {
			json.Unmarshal(m.Data, &event)
			subscriber.lastReceived = m.Sequence
			exitChannel <- &event
		})
	}(exitChannel)

	return exitChannel, nil
}

func (subscriber *StanSubscriber) ReadEvents(topics []string) ([]*domain.Event, error) {
	var wg sync.WaitGroup
	returnedEvents := make([]*domain.Event, 0)

	for _, topic := range topics {
		wg.Add(1)
		var event domain.Event

		sub, _ := subscriber.Client.Subscribe(topic, func(m *stan.Msg) {
			json.Unmarshal(m.Data, &event)
			returnedEvents = append(returnedEvents, &event)

			//Need this to quit the subscribe
			//ReadEvents must only read all the msgs passed in the stream
			//That's why we need to end the subscribe
			if subscriber.lastReceived == m.Sequence {
				wg.Done()
				return
			}

		}, stan.DeliverAllAvailable())
		wg.Wait()
		sub.Unsubscribe()
	}

	return returnedEvents, nil
}

func (subscriber *StanSubscriber) Close() error {
	return subscriber.Client.Close()
}

func (subscriber *StanSubscriber) IsStreamExist(string) bool {
	return false
}
