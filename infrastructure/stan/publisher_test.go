package stan_test

import (
	"testing"

	"bitbucket.org/ValentinLethiot/natsPoc/domain"
	"bitbucket.org/ValentinLethiot/natsPoc/infrastructure/stan"
	"github.com/stretchr/testify/assert"
)

func TestStanPublisherConnection(t *testing.T) {
	//Given

	//When
	publisher, err := stan.NewStanPublisher("test-cluster", "test")

	//Then
	assert.NoError(t, err)
	assert.NotNil(t, publisher)
}

func TestStanPublisherSendEvent(t *testing.T) {
	//Given
	publisher, _ := stan.NewStanPublisher("test-cluster", "test2")
	evt1 := domain.NewEvent("ApplicationCreated", "Une application a été créé")
	evt2 := domain.NewEvent("ApplicationDeleted", "Une application a été supprimée")

	//When
	err := publisher.Publish("myStream", evt1, evt2)

	//Then
	assert.NoError(t, err)
}

func TestStanPublisherClose(t *testing.T) {
	//Given
	publisher, _ := stan.NewStanPublisher("test-cluster", "test3")

	//When
	err := publisher.Close()

	//Then
	assert.NoError(t, err)
}
