package nats_test

import (
	"testing"

	"bitbucket.org/ValentinLethiot/natsPoc/domain"
	"bitbucket.org/ValentinLethiot/natsPoc/infrastructure/nats"
	"github.com/stretchr/testify/assert"
)

func TestNatsPublisherConnection(t *testing.T) {
	//Given

	//When
	publisher, err := nats.NewNatsPublisher("demo.nats.io", "Nats Publisher example")

	//Then
	assert.NoError(t, err)
	assert.NotNil(t, publisher)
}

func TestNatsPublisherSendEvent(t *testing.T) {
	//Given
	publisher, _ := nats.NewNatsPublisher("demo.nats.io", "Nats Publisher example")
	evt1 := domain.NewEvent("ApplicationCreated", "Une application a été créé")
	evt2 := domain.NewEvent("ApplicationDeleted", "Une application a été supprimée")

	//When
	err := publisher.Publish("myStream", evt1, evt2)

	//Then
	assert.NoError(t, err)
}

func TestNatsPublisherClose(t *testing.T) {
	//Given
	publisher, _ := nats.NewNatsPublisher("demo.nats.io", "Nats Publisher example")

	//When
	err := publisher.Close()

	//Then
	assert.NoError(t, err)
}
