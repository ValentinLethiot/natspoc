package nats

import (
	"bitbucket.org/ValentinLethiot/natsPoc/domain"
	"github.com/nats-io/nats.go"
)

type NatsSubscriber struct {
	Client *nats.EncodedConn
}

func NewNatsSubscriber(address, name string) (*NatsSubscriber, error) {
	conn, err := nats.Connect(address /*"demo.nats.io"*/, nats.Name(name)) //"Nats Subscriber example"))
	if err != nil {
		return nil, err
	}
	enc, err := nats.NewEncodedConn(conn, nats.JSON_ENCODER)
	if err != nil {
		return nil, err
	}
	return &NatsSubscriber{
		Client: enc,
	}, nil
}

func (subscriber *NatsSubscriber) Subscribe(topic string) (<-chan *domain.Event, error) {
	exitChan := make(chan *domain.Event, 0)
	go func(chan *domain.Event) {
		subscriber.Client.Subscribe(topic, func(m *domain.Event) {
			exitChan <- &domain.Event{
				ID:        m.ID,
				Type:      m.Type,
				Timestamp: m.Timestamp,
				Data:      m.Data,
			}
		})
	}(exitChan)

	return exitChan, nil
}

func (subscriber *NatsSubscriber) ReadEvents(topics []string) ([]*domain.Event, error) {
	return []*domain.Event{}, nil
}

func (subscriber *NatsSubscriber) Close() error {
	subscriber.Client.Close()
	return nil
}

func (subscriber *NatsSubscriber) IsStreamExist(string) bool {
	return false
}
