package nats_test

import (
	"testing"

	"bitbucket.org/ValentinLethiot/natsPoc/infrastructure/nats"
	"github.com/stretchr/testify/assert"
)

func TestNatsSubscriberConnection(t *testing.T) {
	//Given

	//When
	subscriber, err := nats.NewNatsSubscriber("test-cluster", "test")

	//Then
	assert.NoError(t, err)
	assert.NotNil(t, subscriber)
}

func TestNatsSubscriberClose(t *testing.T) {
	//Given
	subscriber, _ := nats.NewNatsSubscriber("test-cluster", "test3")

	//When
	err := subscriber.Close()

	//Then
	assert.NoError(t, err)
}
