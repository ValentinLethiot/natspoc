package nats

import (
	"bitbucket.org/ValentinLethiot/natsPoc/domain"
	"github.com/nats-io/nats.go"
)

type NatsPublisher struct {
	Client *nats.EncodedConn
}

func NewNatsPublisher(address, name string) (*NatsPublisher, error) {
	conn, err := nats.Connect(address, nats.Name(name))
	if err != nil {
		return nil, err
	}
	enc, err := nats.NewEncodedConn(conn, nats.JSON_ENCODER)
	if err != nil {
		return nil, err
	}
	return &NatsPublisher{
		Client: enc,
	}, nil
}

func (publisher *NatsPublisher) Publish(topic string, events ...*domain.Event) error {
	for _, event := range events {
		err := publisher.Client.Publish(topic, event)
		if err != nil {
			return err
		}
	}
	return nil
}

func (publisher *NatsPublisher) Close() error {
	publisher.Client.Close()
	return nil
}
