package infrastructure

import (
	"bitbucket.org/ValentinLethiot/natsPoc/domain"
)

type Publisher interface {
	Publish(topic string, events ...*domain.Event) error
	Close() error
}

type Subscriber interface {
	Subscribe(topic string) (<-chan *domain.Event, error)
	ReadEvents(topics []string) ([]*domain.Event, error)
	Close() error
	IsStreamExist(string) bool
}
