package eventStore

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"

	"bitbucket.org/ValentinLethiot/natsPoc/domain"
	"github.com/jdextraze/go-gesclient"
	"github.com/jdextraze/go-gesclient/client"
	uuid "github.com/satori/go.uuid"
)

type EventStorePublisher struct {
	Client client.Connection
}

func NewEventStorePublisher() (*EventStorePublisher, error) {
	uri, _ := url.Parse("tcp://127.0.0.1:1113/")
	settings := client.CreateConnectionSettings().KeepReconnecting().Build()
	conn, err := gesclient.Create(settings, uri, "Publisher")
	if err != nil {
		return nil, err
	}

	conn.Connected().Add(func(evt client.Event) error { log.Printf("Connected: %+v", evt); return nil })
	conn.Disconnected().Add(func(evt client.Event) error { log.Printf("Disconnected: %+v", evt); return nil })
	conn.Reconnecting().Add(func(evt client.Event) error { log.Printf("Reconnecting: %+v", evt); return nil })
	conn.Closed().Add(func(evt client.Event) error { log.Fatalf("Connection closed: %+v", evt); return nil })
	conn.ErrorOccurred().Add(func(evt client.Event) error { log.Printf("Error: %+v", evt); return nil })
	conn.AuthenticationFailed().Add(func(evt client.Event) error { log.Printf("Auth failed: %+v", evt); return nil })

	if err := conn.ConnectAsync().Wait(); err != nil {
		log.Fatalf("Error connecting: %v", err)
	}

	return &EventStorePublisher{
		Client: conn,
	}, nil
}

func (publisher *EventStorePublisher) Publish(topic string, events ...*domain.Event) error {
	sentEvents := make([]*client.EventData, 0)
	for _, event := range events {
		uuid, _ := uuid.NewV4()
		jsonEvent, _ := json.Marshal(event)
		eventData := client.NewEventData(uuid, event.Type, true, jsonEvent, nil)
		sentEvents = append(sentEvents, eventData)
	}

	task, err := publisher.Client.AppendToStreamAsync(topic, client.ExpectedVersion_Any, sentEvents, nil)
	if err != nil {
		return fmt.Errorf("Error occured while appending to stream: %v", err)
	} else if err := task.Error(); err != nil {
		return fmt.Errorf("Error occured while waiting for result of appending to stream: %v", err)
	}
	return nil
}

func (publisher *EventStorePublisher) Close() error {
	return publisher.Client.Close()
}
