package eventStore

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"

	"bitbucket.org/ValentinLethiot/natsPoc/domain"
	"github.com/jdextraze/go-gesclient"
	"github.com/jdextraze/go-gesclient/client"
)

type EventStoreSubscriber struct {
	Client client.Connection
}

func NewEventStoreSubscriber() (*EventStoreSubscriber, error) {
	uri, _ := url.Parse("tcp://127.0.0.1:1113/")
	settings := client.CreateConnectionSettings().KeepReconnecting().Build()
	conn, err := gesclient.Create(settings, uri, "Subscriber")
	if err != nil {
		return nil, err
	}

	conn.Connected().Add(func(evt client.Event) error { log.Printf("Connected: %+v", evt); return nil })
	conn.Disconnected().Add(func(evt client.Event) error { log.Printf("Disconnected: %+v", evt); return nil })
	conn.Reconnecting().Add(func(evt client.Event) error { log.Printf("Reconnecting: %+v", evt); return nil })
	conn.Closed().Add(func(evt client.Event) error { log.Fatalf("Connection closed: %+v", evt); return nil })
	conn.ErrorOccurred().Add(func(evt client.Event) error { log.Printf("Error: %+v", evt); return nil })
	conn.AuthenticationFailed().Add(func(evt client.Event) error { log.Printf("Auth failed: %+v", evt); return nil })

	if err := conn.ConnectAsync().Wait(); err != nil {
		log.Fatalf("Error connecting: %v", err)
	}

	return &EventStoreSubscriber{
		Client: conn,
	}, nil
}

func (subscriber *EventStoreSubscriber) Subscribe(topic string) (<-chan *domain.Event, error) {
	exitChannel := make(chan *domain.Event)

	task, err := subscriber.Client.SubscribeToStreamAsync(topic, true, eventReceivedHandler(exitChannel), subscriptionDropped, nil)
	if err != nil {
		return nil, fmt.Errorf("Error occured while subscribing to stream: %v", err)
	} else if err := task.Error(); err != nil {
		return nil, fmt.Errorf("Error occured while waiting for result of subscribing to stream: %v", err)
	} else {
		sub := task.Result().(client.EventStoreSubscription)
		log.Printf("SubscribeToStream result: %+v", sub)
	}
	return exitChannel, nil
}

func eventReceivedHandler(exitChannel chan *domain.Event) func(client.EventStoreSubscription, *client.ResolvedEvent) error {
	return func(_ client.EventStoreSubscription, e *client.ResolvedEvent) error {
		var event domain.Event
		json.Unmarshal(e.Event().Data(), &event)
		exitChannel <- &event
		return nil
	}
}

func subscriptionDropped(_ client.EventStoreSubscription, r client.SubscriptionDropReason, err error) error {
	log.Printf("subscription dropped: %s, %v", r, err)
	return nil
}

func (subscriber *EventStoreSubscriber) ReadEvents(topics []string) ([]*domain.Event, error) {
	returnedEvents := make([]*domain.Event, 0)
	var receivedEvent domain.Event

	for _, topic := range topics {
		task, err := subscriber.Client.ReadStreamEventsForwardAsync(topic, 0, 100, false, nil)
		if err != nil {
			return nil, err
		} else if err := task.Error(); err != nil {
			return nil, err
		}
		result := task.Result().(*client.StreamEventsSlice)
		events := result.Events()
		for _, event := range events {
			json.Unmarshal(event.Event().Data(), &receivedEvent)
			returnedEvents = append(returnedEvents, &receivedEvent)
		}
	}
	return returnedEvents, nil
}

func (subscriber *EventStoreSubscriber) Close() error {
	return subscriber.Client.Close()
}

func (subscriber *EventStoreSubscriber) IsStreamExist(topic string) bool {
	task, err := subscriber.Client.ReadStreamEventsForwardAsync(topic, 0, 1, false, nil)
	if err != nil {
		log.Fatalf(err.Error())
		return false
	} else if err := task.Error(); err != nil {
		log.Fatalf(err.Error())
		return false
	}
	result := task.Result().(*client.StreamEventsSlice)
	return result.IsEndOfStream()
}
