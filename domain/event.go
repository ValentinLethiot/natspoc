package domain

import (
	"fmt"
	"time"

	uuid "github.com/satori/go.uuid"
)

type Event struct {
	ID        string `json:"ID"`
	Type      string `json:"Type"`
	Timestamp int64  `json:"Timestamp"`
	Data      string `json:"Data"`
}

func NewEvent(typ, data string) *Event {
	uid, _ := uuid.NewV4()
	id := fmt.Sprint(uid)
	now := time.Now()

	return &Event{
		ID:        id,
		Type:      typ,
		Timestamp: now.Unix(),
		Data:      data,
	}
}
