package main

import (
	"fmt"
	"log"
	"time"

	"bitbucket.org/ValentinLethiot/natsPoc/domain"
	"bitbucket.org/ValentinLethiot/natsPoc/infrastructure/eventStore"
)

var evt1 = domain.NewEvent("ApplicationCreated", "un poc est crée")
var evt2 = domain.NewEvent("ApplicationDeleted", "un poc est supprimé")

func main() {
	pub, err := eventStore.NewEventStorePublisher()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Publisher ready")
	for {
		time.Sleep(3 * time.Second)
		err = pub.Publish("unTopic", evt1, evt2)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Events Sent")
	}
}
