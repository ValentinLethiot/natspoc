package main

import (
	"fmt"

	"bitbucket.org/ValentinLethiot/natsPoc/infrastructure/eventStore"
)

func main() {
	sub, err := eventStore.NewEventStoreSubscriber()
	if err != nil {
		fmt.Println(err)
	}
	channel, err := sub.Subscribe("unTopic")
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Subscriber ready")
	for {
		for msg := range channel {
			fmt.Println(msg)
		}
	}
}
