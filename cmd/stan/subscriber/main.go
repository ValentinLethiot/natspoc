package main

import (
	"fmt"

	"bitbucket.org/ValentinLethiot/natsPoc/infrastructure/stan"
)

func main() {
	sub, err := stan.NewStanSubscriber("test-cluster", "client-subscriber")
	if err != nil {
		fmt.Println(err)
	}

	//Read all the old events
	events, err := sub.ReadEvents([]string{"unTopic"})
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(len(events))

	fmt.Println("Read finished, start to subscribe")
	//Subscribe to the stream
	channel, err := sub.Subscribe("unTopic")
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Subscriber ready")

	go func() {
		for {
			for msg := range channel {
				fmt.Println(msg)
			}
		}
	}()

}
