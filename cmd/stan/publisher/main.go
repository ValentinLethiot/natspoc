package main

import (
	"fmt"
	"time"

	"bitbucket.org/ValentinLethiot/natsPoc/domain"
	"bitbucket.org/ValentinLethiot/natsPoc/infrastructure/stan"
)

func main() {
	pub, err := stan.NewStanPublisher("test-cluster", "client-publisher")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Publisher ready")
	for {
		time.Sleep(1 * time.Second)
		var evt1 = domain.NewEvent("ApplicationCreated", "un poc est crée")
		pub.Publish("unTopic", evt1)
		fmt.Println("Event", evt1.Timestamp, "published")
	}
}
