package main

import (
	"fmt"

	"bitbucket.org/ValentinLethiot/natsPoc/infrastructure/nats"
)

func main() {
	sub, err := nats.NewNatsSubscriber("demo.nats.io", "Nats Publisher example")
	if err != nil {
		fmt.Println(err)
	}
	channel, err := sub.Subscribe("unTopic")
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Subscriber ready")
	for {
		for msg := range channel {
			fmt.Println(msg)
		}
	}
}
