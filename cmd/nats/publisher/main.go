package main

import (
	"fmt"
	"time"

	"bitbucket.org/ValentinLethiot/natsPoc/domain"
	"bitbucket.org/ValentinLethiot/natsPoc/infrastructure/nats"
)

var evt1 = domain.NewEvent("ApplicationCreated", "un poc est crée")
var evt2 = domain.NewEvent("ApplicationDeleted", "un poc est supprimé")

func main() {
	pub, err := nats.NewNatsPublisher("demo.nats.io", "Nats Publisher example")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Publisher ready")
	for {
		time.Sleep(1 * time.Second)
		pub.Publish("unTopic", evt1)
		fmt.Println("Event published")
		time.Sleep(1 * time.Second)
		pub.Publish("unTopic", evt2)
		fmt.Println("Event published")
	}
}
